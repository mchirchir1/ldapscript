Class usrClass
    Private givenName
    Private sn
    Private uid
    Private umucEmplId
    Private umucGUID
    Private umucipPhone
    Private umucCampusPhone
    Private umucWorkSpace
    Private umucStaffSync
    Private eduPersonPrimaryOrgUnitDN
    Private eduPersonPrimaryAffiliation
    
    Property Get firstName()
        firstName = givenName
    End Property

    Property Get lastName()
        lastName = sn
    End Property

    Property Get userid()
        userid = uid
    End Property

    Property Get emplID()
        emplID = umucEmplId
    End Property

    Property Get ldapGUID()
        ldapGUID = umucGUID
    End Property
    
    Property Get ipPhone()
        ipPhone = umucipPhone
    End Property
    
    Property Get campusPhone()
        campusPhone = umucCampusPhone
    End Property
    
    Property Get workSpace()
        workSpace = umucWorkSpace
    End Property
    
    Property Get staffSync()
        staffSync = umucStaffSync
    End Property
    
    Property Get personPrimaryOrgUnitDN()
        personPrimaryOrgUnitDN = eduPersonPrimaryOrgUnitDN
    End Property
    
    Property Get personPrimaryAffiliation()
        personPrimaryAffiliation = eduPersonPrimaryAffiliation
    End Property
    
    Property Let firstName(NewVal)
        givenName = NewVal
    End Property

    Property Let lastName(NewVal)
        sn = NewVal
    End Property

    Property Let userid(NewVal)
        uid = NewVal
    End Property

    Property Let emplID(NewVal)
        umucEmplId = NewVal
    End Property

    Property Let ldapGUID(NewVal)
        umucGUID = NewVal
    End Property
    
    Property Let ipPhone(NewVal)
        umucipPhone = NewVal
    End Property
    
    Property Let campusPhone(NewVal)
        umucCampusPhone = NewVal
    End Property
    
    Property Let workSpace(NewVal)
        umucWorkSpace = NewVal
    End Property
    
    Property Let staffSync(NewVal)
        umucStaffSync = NewVal
    End Property
    
    Property Let personPrimaryOrgUnitDN(NewVal)
        eduPersonPrimaryOrgUnitDN = NewVal
    End Property
    
    Property Let personPrimaryAffiliation(NewVal)
        eduPersonPrimaryAffiliation = NewVal
    End Property
    
End Class

Dim strPassword
Dim sDN
Dim sRoot
Dim sUser
Dim dso
Dim oAuth
Dim oConn
Dim usrObj

Const E_ADS_PROPERTY_NOT_FOUND = &h8000500D
Const ADS_PROPERTY_APPEND = 3
Const ADS_UF_ACCOUNTDISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32
Const ADS_PROPERTY_CLEAR = 1
Const ADS_SECURE_AUTHENTICATION = 1
Const ADS_USE_ENCRYPTION = 2
Const ForAppending = 8
Const ForReading = 1
Const ForWriting = 2
Const OverwriteExisting = True

'******************************************************************************
'User credentials
'******************************************************************************
sUser = "qat-us\svc-adupdate"
sDN = "cn=ADUpdate,ou=applications,dc=umuc,dc=edu"
sRoot = "LDAP://usv-ldpapp-q07.us.umuc.edu"
strPassword = "91!MunguniMoto"
main()

'******************************************************************************
'Main Function
'******************************************************************************
Function main()

Set dso = GetObject("LDAP:")
Set oAuth = dso.OpenDSObject(sRoot, sDN, strPassword, 0)

Set oConn = CreateObject("ADODB.Connection")
oConn.Provider = "ADSDSOObject"
oConn.Open "Ads Provider", sDN, strPassword


Set ofs = createobject("scripting.filesystemobject")
Set ousernameFile = ofs.opentextfile("c:\Scratch\Misc\users.txt" , 1, false)


While not ousernameFile.AtEndOfStream
	useriden = ousernameFile.ReadLine()
	
	'Create new User	
	Set usrObj = New usrClass
	Set usrObjClone = New usrClass

	If IsNumeric(useriden) Then
		vsearch = "(umucemplid=" & useriden & ")"
	Else
		vsearch = "(uid=" & useriden & ")"
	End If
	
Set oRS = oConn.Execute("<LDAP://usv-ldpapp-q07.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";uid,umucEmplID,umucGUID,umucipPhone,umucCampusPhone,umucWorkSpace,eduPersonPrimaryOrgUnitDN,sn,givenName,eduPersonPrimaryAffiliation;subtree")

Set usrObj = New usrClass	
	For Each Element In oRS.Fields(0).Value
		usrObj.userid = Element
	Next
	WScript.Echo usrObj.userid
	usrObj.emplID = oRS.Fields(1).Value
	usrObj.ldapGUID = oRS.Fields(2).Value
	
	If IsNull(oRS.Fields(3).Value) Then
		usrObj.ipPhone = "TBD"
	Else
		For Each Element In oRS.Fields(3).Value
			usrObj.ipPhone = Element
		Next
	End If
	
	If IsNull(oRS.Fields(4).Value) Then
		usrObj.campusPhone = "TBD"
	Else
		For Each Element In oRS.Fields(4).Value
			usrObj.campusPhone = Element
		Next
	End If

	If IsNull(oRS.Fields(5).Value) Then
		usrObj.workSpace = "TBD"
	Else
		For Each Element In oRS.Fields(5).Value
			usrObj.workSpace = Element
		Next
	End If

	For Each Element In oRS.Fields(6).Value
			var1 = Element
			var1 = Trim(var1)
			var2 = split(var1,"=")
			var3 = var2(0)
			var4 = var2(1)
			usrObj.personPrimaryOrgUnitDN = var4
	Next
	
	For Each Element In oRS.Fields(7).Value
		var1 = Element
		var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
		var2 = Replace(var2," ","",1,-1)
		usrObj.lastName = var2
	Next
	
	For Each Element In oRS.Fields(8).Value
		var1 = Element
		var1 = Trim(var1)
		var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
		var2 = Replace(var2," ","",1,-1)
		usrObj.firstName = var2
	Next
	
	usrObj.personPrimaryAffiliation = oRS.Fields(9).Value
	
	usrObj.staffSync = "N"
	
If usrObj.personPrimaryOrgUnitDN = "USA" Then
	createAccountUS()
	
ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
	createAccountEurope()
	
ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
	createAccountAsia()
	
Else
	WScript.Echo "The user's organization location couldnt be established, check LDAP eduPersonPrimaryOrgUnitDN"
	
End If
		
Set usrobj = Nothing
Wend

End Function


'******************************************************************************
'Populate user object Asia
'******************************************************************************
Function createAccountAsia()

Set openDS = GetObject("LDAP:")
Set objContainer = openDS.OpenDSObject("LDAP://usv-opsdom-q05.qat-asia.umuc.edu/OU=People,DC=qat-asia,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

displayName = usrObj.firstName & " " & usrObj.lastName
employeeEmail = usrObj.firstName & "." & usrObj.lastName & "@umuc.eduT"
uPN = usrObj.userID & "@qat-asia.umuc.edu"

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "Asia"
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", employeeEmail
objUser.Put "company", "University of Maryland University College"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "ipPhone", usrObj.ipPhone
objUser.Put "physicalDeliveryOfficeName", "Asia"
objUser.Put "telephoneNumber", usrObj.campusPhone
objUser.Put "ipPhone", usrObj.ipPhone
objUser.Put "streetAddress", usrObj.workSpace
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = "Password990"


objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo


End Function

'******************************************************************************
'Populate user object Europe
'******************************************************************************
Function createAccountEurope()

Set openDS = GetObject("LDAP:")
Set objContainer = openDS.OpenDSObject("LDAP://usv-opsdom-q03.qat-europe.umuc.edu/OU=People,DC=qat-europe,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

displayName = usrObj.firstName & " " & usrObj.lastName
employeeEmail = usrObj.firstName & "." & usrObj.lastName & "@umuc.eduT"
uPN = usrObj.userID & "@qat-europe.umuc.edu"

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "Europe"
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", employeeEmail
objUser.Put "company", "University of Maryland University College"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "ipPhone", usrObj.ipPhone
objUser.Put "physicalDeliveryOfficeName", "Europe"
objUser.Put "telephoneNumber", usrObj.campusPhone
objUser.Put "ipPhone", usrObj.ipPhone
objUser.Put "streetAddress", usrObj.workSpace
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = "Password990"


objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

End Function

'******************************************************************************
'Populate user object US
'******************************************************************************
Function createAccountUS()

Set openDS = GetObject("LDAP:")
Set objContainer = openDS.OpenDSObject("LDAP://usv-opsdom-q08.qat-us.umuc.edu/OU=People,DC=qat-us,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

displayName = usrObj.firstName & " " & usrObj.lastName
employeeEmail = usrObj.firstName & "." & usrObj.lastName & "@umuc.eduT"
uPN = usrObj.userID & "@qat-us.umuc.edu"

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "US"
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", employeeEmail
objUser.Put "company", "University of Maryland University College"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "ipPhone", usrObj.ipPhone
objUser.Put "physicalDeliveryOfficeName", "US"
objUser.Put "telephoneNumber", usrObj.campusPhone
objUser.Put "ipPhone", usrObj.ipPhone
objUser.Put "streetAddress", usrObj.workSpace
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = "Password990"


objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

End Function


'******************************************************************************
'Update Sync Flag
'******************************************************************************
Function updateSyncFlag()
On Error Resume Next
'LDAP set flag to synced "Y"


	ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
	
	'Update LDAP
	Set obj = dso.OpenDSObject("LDAP://usv-ldpapp-q07.us.umuc.edu/"& ldapobj,sDN,strPassword,0)
	obj.put "umucStaffSync", usrObj.staffSync
	obj.setinfo
	
End Function

'******************************************************************************
'Update Sync Flag - depracated for QAT testing purposes
'******************************************************************************
Function updateSyncFlagToNo()
On Error Resume Next
'LDAP set flag to synced "Y"


	ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
	
	'Update LDAP
	Set obj = dso.OpenDSObject("LDAP://usv-ldpapp-q07.us.umuc.edu/"& ldapobj,sDN,strPassword,0)
	obj.put "umucStaffSync", usrObj.staffSync
	obj.setinfo
	
End Function